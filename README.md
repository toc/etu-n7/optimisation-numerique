# Optimisation numérique

Cours d'optimisation en dimension finie avec contraintes d'égalité et d'inégalité et algorithmes.

**Remarque préliminaire.** Voir la [documentation générale](https://gitlab.irit.fr/toc/etu-n7/documentation) pour récupérer le cours (clonage d'un projet Git), etc.


## Cours

- [Polycopié](https://gitlab.irit.fr/toc/etu-n7/optimisation-numerique/-/raw/master/cours/optimisation-2A-poly.pdf)
- [Slides](https://gitlab.irit.fr/toc/etu-n7/optimisation-numerique/-/raw/master/cours/optimisation-2A-slides.pdf?ref_type=heads)

## TD

- [TD 1](https://gitlab.irit.fr/toc/etu-n7/optimisation-numerique/-/raw/master/td/td1.pdf) : Optimisation, rappels et généralités
- [TD 2-3](https://gitlab.irit.fr/toc/etu-n7/optimisation-numerique/-/raw/master/td/td2-3.pdf) : Contraintes : égalités, inégalités
- [TD 4](https://gitlab.irit.fr/toc/etu-n7/optimisation-numerique/-/raw/master/td/td4.pdf?ref_type=heads) : Quelques sous-problèmes de régions de confiance
- [TD 5](https://gitlab.irit.fr/toc/etu-n7/optimisation-numerique/-/raw/master/td/td5.pdf?ref_type=heads) : Algorithmie

## TP 

Les sujets de TP se trouvent [ici](https://gitlab.irit.fr/toc/etu-n7/projet-optinum).
